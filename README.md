# Multi AWS Account Kubernetes Cluster Deployment Pipeline
[![pipeline status](https://gitlab.com/arunalakmal/tc-paas-accounts-deployment/badges/master/pipeline.svg)](https://gitlab.com/arunalakmal/tc-paas-accounts-deployment/-/commits/master)

This pipeline was created to deploy a 4 node Kubernetes cluster (1 Master and 3 Workers) from a remote shared GitLab runner. 

Initially, test cluster was deployed using [TC AWS Kubernetes Deployment](https://gitlab.com/arunalakmal/TCAWSKubeDeploy) repository,
and it wanted to extended to multiple accounts to have a test Dev and Prod clusters. (These development created purely for my personal learning only).

Kubernetes installation on Master and Worker nodes also performed by remotely using Ansible and playbooks also run from the remote GitLab runner. 
This can be further exted or configure the Kubernetes installation inside the instances and I did not focused on such configuration as these are my
test clusters (Dev/Prod) which can be accessed over the public internet for a short period of time only for the testing purposes. 

Deploying to the Prod Cluster is paused and not performing automatically after the Dev Cluster deployment. Manual Deployment should be performed to start the deployment. Delete Cluster Deployment also a manual Job. 

AWS credentials and Terraform backend configurations were stored as GitLab variables and, credentials were added as the environment variables 
when the runner starting the job. 

I used a custom built Docker image to run the jobs and all the relevant libraries were installed (ex: Ansible, Terraform, Chef inspec, etc) on the 
Dokcer image itself. The Docker image is located in my [GitHub Repository](https://github.com/ArunaLakmal/Dockerfile-Terraform-Ansible-AWS). 

Terraform backend also, configured separately and for easy access I have created separate [GitHub Repository](https://github.com/ArunaLakmal/Terraform-Backend) which can be used in any Terraform project which I create. 

Chef Inspec tests also added to run integration test against the deployed AWS resources and those are  directly running with an [Inspec Profile](https://github.com/ArunaLakmal/inspec-profile-aws-tc-k8s) in GitHub. These tests will run after the deployment of the AWS resourses. 